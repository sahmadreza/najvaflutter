package com.najva.najvaflutter;

import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.najva.sdk.push_notification.NajvaPushNotificationHandler;

import java.util.concurrent.atomic.AtomicBoolean;

import io.flutter.plugin.common.PluginRegistry;
import io.flutter.view.FlutterCallbackInformation;
import io.flutter.view.FlutterMain;
import io.flutter.view.FlutterNativeView;
import io.flutter.view.FlutterRunArguments;

public class NajvaFlutterMessagingService extends FirebaseMessagingService {

    private static FlutterNativeView backgroundFlutterView;

    private static PluginRegistry.PluginRegistrantCallback pluginRegistrantCallback;

    private static AtomicBoolean isIsolateRunning = new AtomicBoolean(false);

    public static final String SHARED_PREFERENCES_KEY = "najva_flutter";

    public static final String BACKGROUND_SETUP_CALLBACK_HANDLE_KEY = "background_key";

    public static void setPluginRegistrant(PluginRegistry.PluginRegistrantCallback pluginRegistrantCallback) {
        NajvaFlutterMessagingService.pluginRegistrantCallback = pluginRegistrantCallback;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Context backgroundContext = getApplicationContext();
        FlutterMain.ensureInitializationComplete(backgroundContext, null);

        // If background isolate is not running start it.
        if (!isIsolateRunning.get()) {
            SharedPreferences p = backgroundContext.getSharedPreferences(SHARED_PREFERENCES_KEY, 0);
            long callbackHandle = p.getLong(BACKGROUND_SETUP_CALLBACK_HANDLE_KEY, 0);
            startBackgroundIsolate(backgroundContext, callbackHandle);
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        NajvaPushNotificationHandler.handleNewToken(this);
    }

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        NajvaPushNotificationHandler.handleMessage(this,remoteMessage);
    }

    public static void startBackgroundIsolate(Context context, long callbackHandle) {
        FlutterMain.ensureInitializationComplete(context, null);
        String appBundlePath = FlutterMain.findAppBundlePath();
        FlutterCallbackInformation flutterCallback =
                FlutterCallbackInformation.lookupCallbackInformation(callbackHandle);


        // Note that we're passing `true` as the second argument to our
        // FlutterNativeView constructor. This specifies the FlutterNativeView
        // as a background view and does not create a drawing surface.
        backgroundFlutterView = new FlutterNativeView(context, true);
        if (pluginRegistrantCallback == null) {
            throw new RuntimeException("PluginRegistrantCallback is not set.");
        }
        FlutterRunArguments args = new FlutterRunArguments();
        args.bundlePath = appBundlePath;
        args.entrypoint = flutterCallback.callbackName;
        args.libraryPath = flutterCallback.callbackLibraryPath;
        backgroundFlutterView.runFromBundle(args);
        pluginRegistrantCallback.registerWith(backgroundFlutterView.getPluginRegistry());
    }
}
