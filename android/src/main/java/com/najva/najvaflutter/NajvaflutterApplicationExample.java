package com.najva.najvaflutter;

import android.content.Context;


import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.PluginRegistry;

public class NajvaflutterApplicationExample extends FlutterApplication implements PluginRegistry.PluginRegistrantCallback{
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
//        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        NajvaFlutterMessagingService.setPluginRegistrant(this);
    }

    @Override
    public void registerWith(PluginRegistry registry) {
        NajvaflutterPlugin.registerWith(registry.registrarFor("com.najva.najvaflutter.NajvaflutterPlugin"));
    }
}
