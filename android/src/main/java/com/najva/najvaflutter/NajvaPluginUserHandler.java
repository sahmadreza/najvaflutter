package com.najva.najvaflutter;

import com.najva.sdk.UserSubscriptionListener;

/**
 * najva plugin user handler
 * created by shayan
 * simply passing token to the NajvaFlutterPlugin class
 */
public class NajvaPluginUserHandler implements UserSubscriptionListener {

    private INajvaPluginUserHandler iNajvaPluginUserHandler;

    NajvaPluginUserHandler(INajvaPluginUserHandler iNajvaPluginUserHandler) {
        this.iNajvaPluginUserHandler = iNajvaPluginUserHandler;
    }

    @Override
    public void onUserSubscribed(String token) {
        iNajvaPluginUserHandler.najvaUserSubscribed(token);
    }
}
