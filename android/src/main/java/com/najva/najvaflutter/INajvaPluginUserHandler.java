package com.najva.najvaflutter;

public interface INajvaPluginUserHandler {
    void najvaUserSubscribed(String token);
}