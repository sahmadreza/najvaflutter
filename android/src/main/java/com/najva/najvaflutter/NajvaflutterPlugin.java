package com.najva.najvaflutter;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;

import com.najva.sdk.Najva;
import com.najva.sdk.NajvaClient;
import com.najva.sdk.NajvaConfiguration;
import com.najva.sdk.NotificationClickListener;
import com.najva.sdk.NotificationReceiveListener;
import com.najva.sdk.UserSubscriptionListener;

import java.util.HashMap;
import java.util.Map;

import io.flutter.Log;
import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;

/**
 * NajvaflutterpluginPlugin
 * created by shayan
 * flutter plugin for implement all features of najva android sdk in flutter
 **/
public class NajvaflutterPlugin implements FlutterPlugin, MethodCallHandler, INajvaPluginUserHandler {

    /**
     * final variables used for method names
     */
    private static final String INIT = "init";
    private static final String GET_SUBSCRIBED_TOKEN = "get_subscribed_token";

    /**
     * final variables for dart method names
     */
    private static final String NEW_JSON_DATA = "onNewJSONData";
    private static final String NEW_USER_SUBSCRIBED = " onUserSubscribed";
    private static final String NEW_NOTIFICATION_RECEIVED = "onNotificationReceived";
    private static final String NEW_NOTIFICATION_CLICKED = "onNotificationClicked";
    private static final String ENABLE_LOCATION = "enable_location";
    private static final String GET_ACTIVITY_JSON = "get_json";
    private static final String FIREBASE = "firebase";
    private static final String SETUP_HANDLER = "setupHandle";
    private static final String API_KEY = "api-key";
    private static final String WEBSITE_ID = "website-id";
    private static final String LOG = "log";

    /**
     * Plugin registration.
     */
    public static void registerWith(PluginRegistry.Registrar registrar) {
        NajvaflutterPlugin plugin = new NajvaflutterPlugin();
        plugin.setContext(registrar.context());
        plugin.onAttachToEngine(registrar.context(), registrar.messenger());
    }

    /* class implementation */

    private MethodChannel channel;
    private Context context;
    private NajvaConfiguration configuration;

    public NajvaflutterPlugin() {

    }

    public void setChannel(MethodChannel channel) {
        this.channel = channel;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void onAttachToEngine(Context context, BinaryMessenger messenger) {
        setContext(context);
        channel = new MethodChannel(messenger, "najvaFlutterplugin");
        channel.setMethodCallHandler(this);
        configuration = new NajvaConfiguration();
    }


    @Override
    public void onMethodCall(MethodCall call, Result result) {
        switch (call.method) {
            case INIT:
                Log.d("NajvaFlutter", call.arguments.toString());
                Map<String, Object> args = (Map<String, Object>) call.arguments;

                Long handleCode = (Long) args.get(SETUP_HANDLER);
                String apiKey = (String) args.get(API_KEY);
                int websiteId = (int) args.get(WEBSITE_ID);

                result.success(init(handleCode, apiKey, websiteId));
                break;
            case GET_SUBSCRIBED_TOKEN:
                result.success(getSubscribedToken());
                break;
            case ENABLE_LOCATION:
                enableLocation();
                result.success(null);
                break;
            case GET_ACTIVITY_JSON:
                result.success(getActivityJson());
                break;
            case FIREBASE:
                Map<String, String> args1 = (Map<String, String>) call.arguments;
                boolean enabled = args1.get("enabled").equalsIgnoreCase("true");
                setFirebaseEnabled(enabled);
                result.success(true);
                break;
            case LOG:
                Map<String, String> args2 = (Map<String, String>) call.arguments;
                boolean enabled2 = args2.get("enabled").equalsIgnoreCase("true");
                setLogEnabled(enabled2);
                result.success(true);
            default:
                result.notImplemented();
                break;
        }
    }

    private void setLogEnabled(boolean enabled) {
        NajvaClient.getInstance().setLogEnabled(enabled);
    }

    private void setFirebaseEnabled(boolean enabled) {
        configuration.setFirebaseEnabled(enabled);
    }

    private String getActivityJson() {
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            if (activity.getIntent().getExtras() != null)
                return activity.getIntent().getExtras().getString(Najva.NOTIFICATION_JSON);
        }
        return null;
    }

    private void enableLocation() {
        configuration.enableLocation();
    }

    private void handleUsersToken() {
        initUserHandler(new NajvaPluginUserHandler(this));
    }

    private void initUserHandler(UserSubscriptionListener handler) {
        configuration.setUserSubscriptionListener(handler);
    }

    private boolean init(Long handleCode, String apiKey, int websiteId) {
        handleUsersToken();

        configuration.setNotificationClickListener(new NotificationClickListener() {
            @Override
            public void onClickNotification(String uuid) {
                Map<String, String> data = new HashMap<>();
                data.put("uuid", uuid);
                channel.invokeMethod(NEW_NOTIFICATION_CLICKED, data);
            }
        });

        configuration.setReceiveNotificationListener(new NotificationReceiveListener() {
            @Override
            public void onReceiveNotification(String notificationId) {
                Map<String, String> data = new HashMap<>();
                data.put("notification_id", notificationId);
                channel.invokeMethod(NEW_NOTIFICATION_RECEIVED, data);
            }
        });

        Context app = context.getApplicationContext();
        if (app instanceof Application) {
            Log.d("NajvaFlutter", "registering najva");
            ((Application) app).registerActivityLifecycleCallbacks(
                    NajvaClient.getInstance(context.getApplicationContext(), configuration, apiKey, websiteId)
            );
        }

        // set handler code
        context.getSharedPreferences(NajvaFlutterMessagingService.SHARED_PREFERENCES_KEY, 0)
                .edit()
                .putLong(NajvaFlutterMessagingService.BACKGROUND_SETUP_CALLBACK_HANDLE_KEY, handleCode)
                .apply();

        return true;
    }

    /**
     * calls method on dart code when new JSON data has been received
     *
     * @param data is JSONObject has received from najva service
     */
    private void onJSONDataReceived(String data) {
        channel.invokeMethod(NEW_JSON_DATA, data);
    }

    @Override
    public void najvaUserSubscribed(String token) {
        channel.invokeMethod(NEW_USER_SUBSCRIBED, token);
    }

    private String getSubscribedToken() {
        return NajvaClient.getInstance().getSubscribedToken();
    }

    @Override
    public void onAttachedToEngine(@NonNull FlutterPluginBinding binding) {
        onAttachToEngine(binding.getApplicationContext(), binding.getBinaryMessenger());
    }

    @Override
    public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {

    }
}
