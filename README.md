# najvaflutter

Unofficial flutter plugin for using najvaSDK in flutter projects.(Android)

Main flutter plugin docs: 
https://doc.najva.com/docs/android/flutter/flutter

# Installation

## Flutter Code Changes:

To add the Najva plugin, add the following code to the ```dependencies``` section of the ```pubspecs.yaml``` file.
```yaml
dependencies:
  najvaflutter:
      git:
        url: https://gitlab.com/sahmadreza/najvaflutter.git
```
To use the Najva plugin in your dart code, create a class called Najva and inherit from the Najvaflutter class.
Then in the constructor of this class call the init method:
```dart
import 'package:najvaflutter/najvaflutter.dart';

var najva;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  najva = NajvaFlutter();
  najva.setFirebaseEnabled(false); // set true if your app using firebase beside najva.
  najva.init(API_KEY, WEBSITE_ID);

  runApp(MyApp());
}
```
The Najva library will start working by calling the init function and passing the appropriate values, and you can receive a notification.

You can get the **API_KEY** and **WEBSITE_ID** values in the Najva user panel, Settings section.

## Android Code Changes:

First, enter the Android folder and open the 'build.gradle' file related to the application module and add the following lines in the dependencies section.
```
dependencies {
  implementation 'com.google.firebase:firebase-messaging:21.1.0'
}
```
Enter the Android folder in your Flutter project and create a class called Application in the application source path and insert the following code into the class:
```java
import com.najva.najvaflutter.NajvaFlutterMessagingService;
import com.najva.najvaflutter.NajvaflutterPlugin;

import io.flutter.app.FlutterApplication;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class Application extends FlutterApplication implements PluginRegistrantCallback {
    @Override
    public void onCreate() {
        super.onCreate();
        NajvaFlutterMessagingService.setPluginRegistrant(this);
    }

    @Override
    public void registerWith(PluginRegistry registry) {
        NajvaflutterPlugin.registerWith(registry.registrarFor("com.najva.najvaflutter.NajvaflutterPlugin"));
    }
}
```
Then open the ```AndroidManifest.xml``` file and add the following line to it.
```xml
<application
        android:name=".Application" 
        android:label="najvaflutter_example"
        android:icon="@mipmap/ic_launcher"
        tools:ignore="GoogleAppIndexingWarning">
```
Then if you are using flutter android embedding v2 change the ```MainActivity``` class as shown below.
```java
package [YOUR_PACKAGE_HERE];
import io.flutter.embedding.android.FlutterActivity;
import io.flutter.embedding.engine.FlutterEngine;
import com.najva.najvaflutter.NajvaflutterPlugin;

public class MainActivity extends FlutterActivity {
    @Override
    public void configureFlutterEngine(FlutterEngine flutterEngine) {
        flutterEngine.getPlugins().add(new NajvaflutterPlugin());
    }
}
```

# Example main.dart

```dart

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:najvaflutter/najvaflutter.dart';
import 'package:http/http.dart' as http;

late NajvaFlutter najva;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  najva = NajvaFlutter();
  najva.enableLocation();
  najva.setFirebaseEnabled(false);
  najva.init("8b84ad3a-3daa-4520-9adc-d7528ea95a54", 12383);
  najva.setOnUserSubscribedListener(onUserSubscribed);
  najva.setOnNotificationReceivedListener(onNotificationReceived);
  najva.setOnNotificationClickListener(onNotificationClicked);
  runApp(MyApp());
}

void onNotificationClicked(String? uuid) {}

void onNotificationReceived(String? id) {}

void onUserSubscribed(String? token) {
  print("Najva user subscribed token : $token");
}

void onJson(String json) {
  print(json);
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _token = 'Unknown';

  @override
  void initState() {
    super.initState();
    najva.getSubscribedToken().then((value) => _token = value ?? "Unknown");
  }

  void onButtonClicked() async {
    final http.Response response  = await http.post(Uri.parse("https://app.najva.com/notification/api/v1/notifications/"),
        headers: <String,String>{'Authorization': 'Token aff3c68af97bf4608fdf28673ca26201ca027ed9', 'Content-Type':'Application/json'},
        body:jsonEncode(<String,dynamic>{
          'title': 'notification from api',
          'body': 'Testing flutter sample',
          'url': 'https://najva.com',
          'onclick_action': 'open-link',
          'icon': 'https://doc.najva.com/img/najva.png',
          'image':'https://doc.najva.com/img/najva.png',
          'priority':'high',
          'api_key': '8b84ad3a-3daa-4520-9adc-d7528ea95a54',
          'subscriber_tokens': <String?> [await najva.getSubscribedToken()]
        }));
    print(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Text("Najva Token: $_token"),
              ElevatedButton(
                  child: Text("get a notification"), onPressed: onButtonClicked)
            ],
          ),
        ),
      ),
    );
  }
}

```

## Flutter Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.