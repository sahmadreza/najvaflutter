import 'dart:async';
import 'dart:ui';

import 'package:flutter/widgets.dart';

import 'package:flutter/services.dart';

void backgroundHandle() {
  WidgetsFlutterBinding.ensureInitialized();
}

class NajvaFlutter {
  ///final variables used for method names
  static const String INIT = "init";
  static const String GET_SUBSCRIBED_TOKEN = "get_subscribed_token";

  ///final variables for dart method names
  static const String NEW_JSON_DATA = "onNewJSONData";
  static const String NEW_USER_SUBSCRIBED = " onUserSubscribed";
  static const String NEW_NOTIFICATION_RECEIVED = "onNotificationReceived";
  static const String NEW_NOTIFICATION_CLICKED = "onNotificationClicked";
  static const String GET_CACHED_JSON_DATA = "getCachedJsonData";
  static const String ENABLE_LOCATION = "enable_location";
  static const String GET_ACTIVITY_JSON = "get_json";
  static const String FIREBASE = "firebase";
  static const String API_KEY = "api-key";
  static const String WEBSITE_ID = "website-id";
  static const String LOG = "log";

  static const String SETUP_HANDLER = "setupHandle";

  Function(String?)? onUserSubscribed;

  Function(String?)? onNotificationReceived;
  Function(String?)? onNotificationClicked;

  MethodChannel _channel = const MethodChannel('najvaFlutterplugin');

  bool isNajvaInitialized = false;

  Future<dynamic> handler(MethodCall call) async {
    String name = call.method;
    print("NajvaFlutter: ${call.method}");
    switch (name) {
      case NEW_USER_SUBSCRIBED:
        deliverUserSubscription(call.arguments);
        break;
      case NEW_NOTIFICATION_CLICKED:
        print("click: ${call.arguments}");
        String uuid = call.arguments.get("uuid");
        if (onNotificationClicked != null) onNotificationClicked!(uuid);
        break;
      case NEW_NOTIFICATION_RECEIVED:
        print("receive: ${call.arguments}");
        if (onNotificationReceived != null)
          onNotificationReceived!(call.arguments);
        break;
    }
  }

  void setOnUserSubscribedListener(Function(String?) onUserSubscribed) {
    this.onUserSubscribed = onUserSubscribed;
  }

  void setOnNotificationReceivedListener(
      Function(String?) onNotificationReceived) {
    this.onNotificationReceived = onNotificationReceived;
  }

  void setOnNotificationClickListener(Function(String?) onNotificationClicked) {
    this.onNotificationClicked = onNotificationClicked;
  }

  Future<void> init(String apiKey, int websiteId) async {
    try {
      _channel.setMethodCallHandler(handler);
      final CallbackHandle? _backgroundHandle =
          PluginUtilities.getCallbackHandle(backgroundHandle);
      if (_backgroundHandle != null) {
        isNajvaInitialized =
            await _channel.invokeMethod(INIT, <String, dynamic>{
          SETUP_HANDLER: _backgroundHandle.toRawHandle(),
          API_KEY: apiKey,
          WEBSITE_ID: websiteId
        });
      }
    } on PlatformException catch (e) {
      print(e.toString());
    }
  }

  void setLogEnabled(bool enabled) {
    _channel.invokeMethod(LOG, <String, String>{'enabled': enabled.toString()});
  }

  void setFirebaseEnabled(bool enabled) {
    _channel.invokeMethod(
        FIREBASE, <String, String>{'enabled': enabled.toString()});
  }

  void deliverUserSubscription(arguments) {
    if (onUserSubscribed != null) onUserSubscribed!(arguments);
  }

  Future<String?> getSubscribedToken() async {
    String? token = await _channel.invokeMethod(GET_SUBSCRIBED_TOKEN);
    return token;
  }

  Future<String?> getActivityJson() async {
    String? json = await _channel.invokeMethod(GET_ACTIVITY_JSON);
    return json;
  }

  void enableLocation() {
    _channel.invokeMethod(ENABLE_LOCATION);
  }
}