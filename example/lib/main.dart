import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:najvaflutter/najvaflutter.dart';
import 'package:http/http.dart' as http;

late NajvaFlutter najva;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  najva = NajvaFlutter();
  najva.enableLocation();
  najva.setFirebaseEnabled(false);
  najva.init("8b84ad3a-3daa-4520-9adc-d7528ea95a54", 12383);
  najva.setOnUserSubscribedListener(onUserSubscribed);
  najva.setOnNotificationReceivedListener(onNotificationReceived);
  najva.setOnNotificationClickListener(onNotificationClicked);
  runApp(MyApp());
}

void onNotificationClicked(String? uuid) {}

void onNotificationReceived(String? id) {}

void onUserSubscribed(String? token) {
  print("Najva user subscribed token : $token");
}

void onJson(String json) {
  print(json);
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _token = 'Unknown';

  @override
  void initState() {
    super.initState();
    najva.getSubscribedToken().then((value) => _token = value ?? "Unknown");
  }

  void onButtonClicked() async {
    final http.Response response  = await http.post(Uri.parse("https://app.najva.com/notification/api/v1/notifications/"),
        headers: <String,String>{'Authorization': 'Token aff3c68af97bf4608fdf28673ca26201ca027ed9', 'Content-Type':'Application/json'},
        body:jsonEncode(<String,dynamic>{
          'title': 'notification from api',
          'body': 'Testing flutter sample',
          'url': 'https://najva.com',
          'onclick_action': 'open-link',
          'icon': 'https://doc.najva.com/img/najva.png',
          'image':'https://doc.najva.com/img/najva.png',
          'priority':'high',
          'api_key': '8b84ad3a-3daa-4520-9adc-d7528ea95a54',
          'subscriber_tokens': <String?> [await najva.getSubscribedToken()]
        }));
    print(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: <Widget>[
              Text("Najva Token: $_token"),
              ElevatedButton(
                  child: Text("get a notification"), onPressed: onButtonClicked)
            ],
          ),
        ),
      ),
    );
  }
}
