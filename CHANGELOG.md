## 1.3.3-null-safety-02

* Upgrade firebase-messaging to version 21.1.0

## 1.3.3-null-safety-01

* Fix some issue

## 1.3.3-null-safety

* Migrate to null-safety
